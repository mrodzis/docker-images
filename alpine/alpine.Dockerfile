FROM alpine:3.13

RUN apk add zip unzip graphviz curl jq openssh
RUN apk add openjdk11-jre --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community
