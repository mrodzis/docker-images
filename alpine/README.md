# Fedora Image

## Build

```bash
docker build --file alpine.Dockerfile -t docker.gitlab.gwdg.de/mrodzis/docker-images/alpine .
```

## Push

```bash
docker push docker.gitlab.gwdg.de/mrodzis/docker-images/alpine
```

## Currently used in the following repositories

- subugoe/ahiqar/Collatex
- subugoe/ahiqar/backend
