# Fedora Image

## Build

```bash
docker build --file alpine-exist-upload.Dockerfile -t docker.gitlab.gwdg.de/mrodzis/docker-images/alpine-exist-upload .
```

## Push

```bash
docker push docker.gitlab.gwdg.de/mrodzis/docker-images/alpine-exist-upload
```

## Currently used in the following repositories

- subugoe/ahiqar/backend
