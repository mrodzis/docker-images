FROM debian:10-slim

RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get install -y zip unzip curl git openssh-client jq