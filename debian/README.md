# Fedora Image

## Build

```bash
docker build --file debian.Dockerfile -t docker.gitlab.gwdg.de/mrodzis/docker-images/debian .
```

## Push

```bash
docker push docker.gitlab.gwdg.de/mrodzis/docker-images/debian
```

## Currently used in the following repositories

- subugoe/ahiqar/backend
