# Fedora Image

## Build

```bash
docker build --file alpine-exist-unit-tests.Dockerfile -t docker.gitlab.gwdg.de/mrodzis/docker-images/alpine-exist-unit-tests .
```

## Push

```bash
docker push docker.gitlab.gwdg.de/mrodzis/docker-images/alpine-exist-unit-tests
```

## Currently used in the following repositories

- subugoe/ahiqar/backend
